package online.cryptopie.app;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Data
@Configuration
@ConfigurationProperties(prefix = "qiwi.props")
public class QiwiProperties {
    public final static String terminal_id = "1949";

    public final static String password = "abprxpx9ha";

    public final static String service_id = "32911";

    public final static Integer WORKER_ID = 1;
}

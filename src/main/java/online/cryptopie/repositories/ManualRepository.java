package online.cryptopie.repositories;

import online.cryptopie.models.Account;
import online.cryptopie.models.Currency;
import online.cryptopie.models.Gate;
import online.cryptopie.models.qiwi.ping.BalanceOvd;
import online.cryptopie.models.transaction.Balance;
import online.cryptopie.models.transaction.Status;
import online.cryptopie.models.transaction.Transaction;
import online.cryptopie.models.transaction.Type;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public class ManualRepository {
    private final MongoTemplate mongoTemplate;
    private final CurrenciesRepository currenciesRepository;
    private final GatesRepository gatesRepository;
    private final TransactionsRepository transactionsRepository;

    @Autowired
    public ManualRepository(MongoTemplate mongoTemplate, CurrenciesRepository currenciesRepository, GatesRepository gatesRepository, TransactionsRepository transactionsRepository) {
        this.mongoTemplate = mongoTemplate;
        this.currenciesRepository = currenciesRepository;

        this.gatesRepository = gatesRepository;
        this.transactionsRepository = transactionsRepository;
    }

    public Gate findAndModifyGateSub(ObjectId gateId, Balance balance) {
        Currency currency = currenciesRepository.findByCurrency(balance.getCurrency());
        Query findAndModifyGate = new Query(Criteria
                .where("_id").is(gateId)
                .and("operationBalance.amount").gte(balance.getAmount().doubleValue())
                .and("operationBalance.currency").is(currency.getId()));
        Update update = new Update();
        update.inc("operationBalance.amount", -balance.getAmount().doubleValue());

        return mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);
    }

    public Account findAndModifyAccountAdd(String accountId, Balance balance) {
        Currency currency = currenciesRepository.findByCurrency(balance.getCurrency());
        Query findAndModifyAccount = new Query(Criteria
                .where("accountId").is(accountId)
                .and("currency").is(currency.getId()));
        Update update = new Update();
        update.inc("balance.amount", balance.getAmount().doubleValue());

        return mongoTemplate.findAndModify(findAndModifyAccount, update, Account.class);
    }

    public Gate findAndModifyGateAdd(ObjectId gateId, Balance balance) { //TODO логика exchange?
        Gate gate = gatesRepository.findById(gateId).get();
        Currency currency = currenciesRepository.findByCurrency(balance.getCurrency());
        Query findAndModifyGate = new Query(Criteria.where("_id").is(gateId)
                .and("operationBalance.currency").is(currency.getId())
                .and("balance.amount").gte(gate.getOperationBalance().getAmount().doubleValue() + balance.getAmount().doubleValue()));
        Update update = new Update();
        update.inc("operationBalance.amount", balance.getAmount().doubleValue());

        return mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);
    }

    public void findAndUpdateGateCommissionAndAccount(ObjectId gateId, Balance balance) {
        Currency currency = currenciesRepository.findByCurrency(balance.getCurrency());
        Query findAndModifyGate = new Query(Criteria.where("_id").is(gateId)
                .and("commissionBalance.currency").is(currency.getId()));
        Update update = new Update();
        update.inc("commissionBalance.amount", balance.getAmount().doubleValue());

        Optional<Gate> gate = gatesRepository.findById(gateId);

        Query findAndModifyAccount = null;
        if (gate.isPresent())
            findAndModifyAccount = new Query(Criteria.where("accountId").is(gate.get().getAccount())
                    .and("balance.currency").is(currency.getId()));

        Update updateAccount = new Update();
        updateAccount.inc("balance.amount", balance.getAmount().doubleValue());

        assert findAndModifyAccount != null;
        Account accountUpdated = mongoTemplate.findAndModify(findAndModifyAccount, updateAccount, Account.class);
        assert accountUpdated != null;
        Transaction transaction = Transaction.builder()
                .gateId(gateId)
                .date(new Date())
                .accountId(accountUpdated.getAccountId())
                .status(Status.SUCCESS)
                .type(Type.COMMISSION)
                .amount(balance)
                .finalAmount(balance)
                .build();
        Gate updated = mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);
        if (updated != null && balance.getAmount().doubleValue() != 0) {
            transactionsRepository.insert(transaction);
        }
    }

    public List<Transaction> findByStatusQIWITransactions(List<ObjectId> qiwiGates, Integer status) {
        List<Transaction> transactions = transactionsRepository.findAllByStatus(status);

        List<Transaction> filteredTransactions = new ArrayList<>();
        if (!transactions.isEmpty())
            transactions.stream().filter(transaction -> {

                for (ObjectId qiwiGateId : qiwiGates) {
                    if (transaction.getGateId() != null)
                        if (transaction.getGateId().equals(qiwiGateId))
                            return true;
                }
                return false;
            }).forEach(filteredTransactions::add);

        return filteredTransactions;
    }


    public Gate findByCodeAndWorkerIdAndModifyBalance(BalanceOvd balanceOvd, Integer workerId) {
        Query find = new Query(Criteria.where("code").is(Integer.valueOf(balanceOvd.getCode())).and("workerId").is(workerId));
        Update update = new Update();
        BigDecimal bigDecimal = new BigDecimal(balanceOvd.getValue());
        update.set("balance.amount", bigDecimal.doubleValue());

        return mongoTemplate.findAndModify(find, update, Gate.class);
    }
}

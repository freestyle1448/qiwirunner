package online.cryptopie.repositories;

import online.cryptopie.models.AutomaticGate;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AutomaticGatesRepository extends MongoRepository<AutomaticGate, ObjectId> {
    AutomaticGate findByGroupName(String groupName);
}

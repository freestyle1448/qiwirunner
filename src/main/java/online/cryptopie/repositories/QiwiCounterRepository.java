package online.cryptopie.repositories;

import online.cryptopie.models.Counter;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface QiwiCounterRepository extends MongoRepository<Counter, ObjectId> {
}

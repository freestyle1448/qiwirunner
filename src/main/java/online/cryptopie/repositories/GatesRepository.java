package online.cryptopie.repositories;

import online.cryptopie.models.Gate;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GatesRepository extends MongoRepository<Gate, ObjectId> {
    Gate findByIdAndCurrency(ObjectId id, ObjectId currency);

    Gate findByIdAndBalance_AmountIsGreaterThanEqualAndCurrency(ObjectId gateId, Number amount, ObjectId currency);

    Gate findByCode(Integer code);

    Gate findByCodeAndWorkerId(Integer code, Integer workerId);
}

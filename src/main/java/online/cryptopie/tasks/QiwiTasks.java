package online.cryptopie.tasks;

import online.cryptopie.app.Locker;
import online.cryptopie.models.AutomaticGate;
import online.cryptopie.models.transaction.History;
import online.cryptopie.models.transaction.Transaction;
import online.cryptopie.repositories.AutomaticGatesRepository;
import online.cryptopie.repositories.ManualRepository;
import online.cryptopie.service.QiwiService;
import org.bson.types.ObjectId;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static online.cryptopie.app.Application.HASH_PAS;
import static online.cryptopie.app.Locker.*;
import static online.cryptopie.models.transaction.Status.IN_PROCESS;
import static online.cryptopie.models.transaction.Status.WAITING;

@Component
@Async
public class QiwiTasks {
    private final ManualRepository manualRepository;
    private final AutomaticGatesRepository automaticGatesRepository;
    private final QiwiService qiwiService;
    private final Locker locker = Locker.getInstance();

    public QiwiTasks(ManualRepository manualRepository, AutomaticGatesRepository automaticGatesRepository, QiwiService qiwiService) {
        this.manualRepository = manualRepository;
        this.automaticGatesRepository = automaticGatesRepository;
        this.qiwiService = qiwiService;
    }

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder();
        for (byte aHash : hash) {
            String hex = Integer.toHexString(0xff & aHash);
            if (hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    public static String genHash(Transaction transaction) {
        if (transaction.getTransactionNumber() == null)
            transaction.setTransactionNumber(0L);


        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        assert digest != null;

        byte[] encodedhash = digest.digest(
                String.format("%s|%s|%s|%s|%s",
                        transaction.getCardNumber(), transaction.getDate(),
                        transaction.getAmount().getAmount().doubleValue()
                        , transaction.getFinalAmount().getAmount().doubleValue(), HASH_PAS).getBytes(StandardCharsets.UTF_8));

        return bytesToHex(encodedhash);
    }

    @Scheduled(fixedRate = 60000)
    public void ping() {
        if (locker.isNotLocked(PAY_LOCK) && locker.isNotLocked(CONFIRM_LOCK)) {
            locker.lock(PING_LOCK);
            qiwiService.ping();
        }
    }

    //TODO лимитировать количество транзакций
    @Scheduled(fixedRate = 10000)
    public void pay() {
        AutomaticGate automaticGates = automaticGatesRepository.findByGroupName("QIWI");
        List<ObjectId> qiwiGates = new ArrayList<>(automaticGates.getGates());
        List<Transaction> transactions = manualRepository.findByStatusQIWITransactions(qiwiGates, WAITING);

        if (!transactions.isEmpty()) {
            if (locker.isNotLocked(PING_LOCK) && locker.isNotLocked(PAY_LOCK)) {
                locker.lock(PAY_LOCK);

                for (Transaction transaction : transactions) {
                    transaction.setStatus(IN_PROCESS);
                    if (transaction.getHistoryList() == null) {
                        transaction.setHistoryList(new ArrayList<>());
                        transaction.getHistoryList().add(History.builder().date(new Date()).prevStatus(WAITING).curStatus(IN_PROCESS).build());
                    }

                    System.err.println(transaction.getHash() + " " + genHash(transaction));
                    if (/*transaction.getHash().equals(genHash(transaction))*/ true) { //TODO поменять
                        qiwiService.saveTransaction(transaction);
                        qiwiService.createQiwiRequest(transaction);
                    } else {
                        qiwiService.declineTransaction(transaction);
                    }

                }
            }
        }

        locker.unlock(PAY_LOCK);
    }

    @Scheduled(fixedRate = 10000)
    public void checkTransactionsOnQiwi() {
        AutomaticGate automaticGates = automaticGatesRepository.findByGroupName("QIWI");
        List<ObjectId> qiwiGates = new ArrayList<>(automaticGates.getGates());
        List<Transaction> transactions = manualRepository.findByStatusQIWITransactions(qiwiGates, IN_PROCESS);

        if (!transactions.isEmpty()) {
            if (locker.isNotLocked(PING_LOCK)) {
                locker.lock(CONFIRM_LOCK);

                for (Transaction transaction : transactions) {
                    if (/*transaction.getHash().equals(genHash(transaction))*/ true) { //TODO поменять
                        qiwiService.saveTransaction(transaction);
                        qiwiService.statusRequest(transaction);
                    } else {
                        qiwiService.declineTransaction(transaction);
                    }
                }
                locker.unlock(CONFIRM_LOCK);
            }
        }
    }
}

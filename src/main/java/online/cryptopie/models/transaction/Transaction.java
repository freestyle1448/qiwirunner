package online.cryptopie.models.transaction;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Document(collection = "transactions")
public class Transaction {
    @Id
    private ObjectId id;
    private String hash;
    private String accountId;
    private ObjectId gateId;
    private Date date;
    private Integer status;
    private String type;
    private Balance amount;
    private Balance commission;
    private String fromAccount;
    private String toAccount;
    private Balance finalAmount;
    private String note;
    private Integer gateType;
    private List<History> historyList;
    private ObjectId toGate;
    private ObjectId fromGate;
    private Balance toAmount;
    private Balance fromAmount;

    private String sign;
    private String salt;
    private String acceptId;

    private String userPhone;
    private String cardNumber;
    private Long transactionNumber;
    private Balance systemAmount;
    private Number rate;
    private Number systemRate;
    private Number limit;
    private Integer remoteStatus;
    private Long split_id;

}

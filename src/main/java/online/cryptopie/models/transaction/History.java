package online.cryptopie.models.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class History {
    private Date date;
    private Integer prevStatus;
    private Integer curStatus;
}

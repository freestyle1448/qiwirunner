package online.cryptopie.models.transaction;

import lombok.*;
import org.javamoney.moneta.Money;

import javax.money.MonetaryAmount;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class Balance {
    private Number amount;
    private String currency;

    public static MonetaryAmount getMonetary(Balance balance) {
        return Money.of(balance.getAmount().doubleValue(), balance.getCurrency());
    }

    public static Balance getBalance(MonetaryAmount amount) {
        return Balance.builder().amount(amount.getNumber().doubleValue()).currency(amount.getCurrency().getCurrencyCode()).build();
    }
}

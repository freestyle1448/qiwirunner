package online.cryptopie.models.qiwi.userResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class ResultCode {
    @XmlAttribute(name = "fatal")
    private Boolean fatal;
    @XmlValue
    private String value;
    @XmlAttribute(name = "message")
    private String message;
    @XmlAttribute(name = "msg")
    private String msg;
}

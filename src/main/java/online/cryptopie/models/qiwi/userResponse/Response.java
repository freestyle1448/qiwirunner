package online.cryptopie.models.qiwi.userResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@XmlRootElement(name = "response")
class Response {
    private String exist;
    private ResultCode result_code;

    @XmlElement(name = "exist")
    public void setExist(String exist) {
        this.exist = exist;
    }

    @XmlElement(name = "result-code")
    public void setResult_code(ResultCode result_code) {
        this.result_code = result_code;
    }
}

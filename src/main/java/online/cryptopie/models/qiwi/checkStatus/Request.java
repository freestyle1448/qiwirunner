package online.cryptopie.models.qiwi.checkStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import online.cryptopie.models.qiwi.Password;

import javax.xml.bind.annotation.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"request_type", "extra_password", "terminal_id", "status"})
@XmlRootElement(name = "request")
public class Request {
    @XmlElement(name = "request-type")
    private String request_type;
    @XmlElement(name = "extra")
    private Password extra_password;
    @XmlElement(name = "terminal-id")
    private String terminal_id;
    @XmlElement(name = "status")
    private Status status;

    public static Request buildRequest(String account_number, String transaction_number, String extra_password, String request_type, String terminal_id) {
        Request request = new Request();
        Status status = new Status();
        PaymentResp payment = new PaymentResp();
        To to = new To();
        to.setAccount_number(account_number);
        payment.setTo(to);
        payment.setTransaction_number(transaction_number);
        status.setPayment(payment);
        request.setStatus(status);
        Password password = new Password();
        password.setPassword("");
        password.setValue(extra_password);
        request.setExtra_password(password);
        request.setRequest_type(request_type);
        request.setTerminal_id(terminal_id);

        return request;
    }
}

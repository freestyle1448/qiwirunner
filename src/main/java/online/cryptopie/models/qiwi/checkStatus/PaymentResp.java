package online.cryptopie.models.qiwi.checkStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@XmlType(propOrder = {"transaction_number", "to"})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "payment")
public class PaymentResp {
    @XmlElement(name = "transaction-number")
    private String transaction_number;
    @XmlElement(name = "to")
    private To to;
}
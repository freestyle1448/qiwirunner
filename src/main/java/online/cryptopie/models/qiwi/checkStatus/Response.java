package online.cryptopie.models.qiwi.checkStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import online.cryptopie.models.qiwi.payResponse.Balance;
import online.cryptopie.models.qiwi.userResponse.ResultCode;

import javax.xml.bind.annotation.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"result_code", "payment", "balances"})
@XmlRootElement(name = "response")
public class Response {
    @XmlElement(name = "result-code")
    private ResultCode result_code;
    @XmlElement(name = "payment")
    private PaymentResp payment;
    @XmlElement(name = "balances")
    @XmlElementWrapper
    private List<Balance> balances;

}

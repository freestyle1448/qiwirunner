package online.cryptopie.models.qiwi.checkStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "payment")
class Payment {
    @XmlAttribute(name = "status")
    private Integer status;
    @XmlAttribute(name = "txn-id")
    private Integer txn_id;
    @XmlAttribute(name = "transaction-number")
    private String transaction_number;
    @XmlAttribute(name = "result-code")
    private Integer result_code;
    @XmlAttribute(name = "final-status")
    private Boolean final_status;
    @XmlAttribute(name = "fatal-error")
    private Boolean fatal_error;
    @XmlAttribute(name = "txn-date")
    private Date txn_date;

}

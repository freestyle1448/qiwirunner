package online.cryptopie.models.qiwi.payResponse;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import online.cryptopie.models.qiwi.pay.From;
import online.cryptopie.models.qiwi.pay.To;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@SuppressWarnings("ALL")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@XmlType(propOrder = {"from", "to"})
@XmlRootElement(name = "payment")
public class ResponsePayment {

    private To to;
    private From from;

    private Integer status;
    private Integer txn_id;
    private String transaction_number;
    private Integer result_code;
    private String message;
    private Boolean final_status;
    private Boolean fatal_error;
    private String txn_date;


    @XmlAttribute(name = "status")
    public void setStatus(Integer status) {
        this.status = status;
    }

    @XmlAttribute(name = "txn-id")
    public void setTxn_id(Integer txn_id) {
        this.txn_id = txn_id;
    }

    @XmlAttribute(name = "transaction-number")
    public void setTransaction_number(String transaction_number) {
        this.transaction_number = transaction_number;
    }

    @XmlAttribute(name = "result-code")
    public void setResultCode(Integer resultCode) {
        this.result_code = resultCode;
    }

    @XmlAttribute(name = "message")
    public void setMessage(String resultCode) {
        this.message = resultCode;
    }


    @XmlAttribute(name = "final-status")
    public void setFinal_status(Boolean final_status) {
        this.final_status = final_status;
    }

    @XmlAttribute(name = "fatal-error")
    public void setFatal_error(Boolean fatal_error) {
        this.fatal_error = fatal_error;
    }

    @XmlAttribute(name = "txn-date")
    public void setTxn_date(String date) {
        this.txn_date = date;
    }


    @XmlElement(name = "to")
    public void setTo(To to) {
        this.to = to;
    }

    @XmlElement(name = "from")
    public void setFrom(From from) {
        this.from = from;
    }

}

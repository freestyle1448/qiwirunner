package online.cryptopie.models.qiwi.payResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import online.cryptopie.models.qiwi.ping.BalanceOvd;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@XmlRootElement(name = "response")
public class QiwiResponse {
    private ResponsePayment payment;
    private List<Balance> balances;

    private List<BalanceOvd> balancesOvd;

    @XmlElement(name = "payment")
    public void setPayment(ResponsePayment payment) {
        this.payment = payment;
    }

    @XmlElementWrapper(name = "balances")
    @XmlElement(name = "balance")
    public void setBalances(List<Balance> balances) {
        this.balances = balances;
    }

    @XmlElementWrapper(name = "balances-ovd")
    @XmlElement(name = "balance-ovd")
    public void setBalancesOvd(List<BalanceOvd> balances) {
        this.balancesOvd = balances;
    }
}

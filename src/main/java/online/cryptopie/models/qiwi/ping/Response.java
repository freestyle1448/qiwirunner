package online.cryptopie.models.qiwi.ping;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import online.cryptopie.models.qiwi.payResponse.Balance;
import online.cryptopie.models.qiwi.userResponse.ResultCode;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@XmlType(propOrder = {"result_code", "balances", "balancesOvd"})
@XmlRootElement(name = "response")

public class Response {
    private ResultCode result_code;

    private List<Balance> balances;

    private List<BalanceOvd> balancesOvd;

    @XmlElement(name = "result-code")
    public void setResult_code(ResultCode result_code) {
        this.result_code = result_code;
    }

    @XmlElementWrapper(name = "balances")
    @XmlElement(name = "balance")
    public void setBalances(List<Balance> balances) {
        this.balances = balances;
    }

    @XmlElementWrapper(name = "balances-ovd")
    @XmlElement(name = "balance-ovd")
    public void setBalancesOvd(List<BalanceOvd> balances) {
        this.balancesOvd = balances;
    }
}

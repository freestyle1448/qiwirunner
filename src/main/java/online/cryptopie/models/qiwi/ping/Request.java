package online.cryptopie.models.qiwi.ping;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import online.cryptopie.models.qiwi.Password;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@XmlType(propOrder = {"request_type", "terminal_id", "extra_password"})
@XmlRootElement(name = "request")
public class Request {
    private String request_type;
    private Password extra_password;
    private String terminal_id;


    public static Request buildRequest(String extra_password, String request_type, String terminal_id) {
        Request request = new Request();

        Password password = new Password();
        password.setPassword("");
        password.setValue(extra_password);
        request.setExtra_password(password);
        request.setRequest_type(request_type);
        request.setTerminal_id(terminal_id);

        return request;
    }

    @XmlElement(name = "request-type")
    private void setRequest_type(String request_type) {
        this.request_type = request_type;
    }

    @XmlElement(name = "extra")
    private void setExtra_password(Password extra_password) {
        this.extra_password = extra_password;
    }

    @XmlElement(name = "terminal-id")
    private void setTerminal_id(String terminal_id) {
        this.terminal_id = terminal_id;
    }

}

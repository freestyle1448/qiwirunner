package online.cryptopie.models.qiwi.userRequest;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@XmlRootElement(name = "extra")
class Phone {
    private String phone;
    private String value;


    @XmlValue
    public void setValue(String value) {
        this.value = value;
    }

    @XmlAttribute(name = "name")
    public void setPhone(String phone) {
        this.phone = "phone";
    }
}

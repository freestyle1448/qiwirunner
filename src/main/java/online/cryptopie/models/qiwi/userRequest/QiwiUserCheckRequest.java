package online.cryptopie.models.qiwi.userRequest;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import online.cryptopie.models.qiwi.Password;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@XmlType(propOrder = {"request_type", "terminal_id", "extra_password", "extra_phone", "extra_ccy"})
@XmlRootElement(name = "request")
public class QiwiUserCheckRequest {
    private String request_type;
    private String terminal_id;

    private Password extra_password;
    private Phone extra_phone;
    private Ccy extra_ccy;

    @XmlElement(name = "request-type")
    private void setRequest_type(String request_type) {
        this.request_type = request_type;
    }

    @XmlElement(name = "terminal-id")
    private void setTerminal_id(String terminal_id) {
        this.terminal_id = terminal_id;
    }

    @XmlElement(name = "extra")
    private void setExtra_password(Password extra_password) {
        this.extra_password = extra_password;
    }

    @XmlElement(name = "extra")
    private void setExtra_phone(Phone extra_phone) {
        this.extra_phone = extra_phone;
    }

    @XmlElement(name = "extra")
    private void setExtra_ccy(Ccy extra_ccy) {
        this.extra_ccy = extra_ccy;
    }

    public static QiwiUserCheckRequest buildRequest(String request_type, String terminal_id,String password, String phone, String ccy) {
        QiwiUserCheckRequest qiwiUserCheckRequest = new QiwiUserCheckRequest();
        qiwiUserCheckRequest.setTerminal_id(terminal_id);
        qiwiUserCheckRequest.setRequest_type(request_type);
        Password passwordP = new Password();
        passwordP.setPassword("");
        passwordP.setValue(password);
        qiwiUserCheckRequest.setExtra_password(passwordP);
        Ccy ccyC = new Ccy();
        ccyC.setCcy("");
        ccyC.setValue(ccy);
        qiwiUserCheckRequest.setExtra_ccy(ccyC);
        Phone phoneP = new Phone();
        phoneP.setPhone("");
        phoneP.setValue(phone);
        qiwiUserCheckRequest.setExtra_phone(phoneP);


        return qiwiUserCheckRequest;
    }
}

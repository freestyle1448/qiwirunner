package online.cryptopie.models.qiwi.pay;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@XmlType(propOrder = {"amount", "ccy", "service_id", "account_number", "account1"})
@XmlRootElement(name = "to")
public class To {
    private String amount;
    private String ccy;
    private String service_id;
    private String account_number;
    private Account1 account1;

    @XmlElement(name = "amount")
    public void setAmount(String amount) {
        this.amount = amount;
    }

    @XmlElement(name = "ccy")
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    @XmlElement(name = "service-id")
    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    @XmlElement(name = "account-number")
    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    @XmlElement(name = "extra")
    public void setAccount1(Account1 account1) {
        this.account1 = account1;
    }
}

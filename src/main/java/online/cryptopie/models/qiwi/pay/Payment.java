package online.cryptopie.models.qiwi.pay;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@XmlType(propOrder = {"transaction_number", "from", "to"})
@XmlRootElement(name = "payment")
public class Payment {
    private String transaction_number;
    private To to;
    private From from;

    @XmlElement(name = "transaction-number")
    public void setTransaction_number(String transaction_number) {
        this.transaction_number = transaction_number;
    }

    @XmlElement(name = "to")
    public void setTo(To to) {
        this.to = to;
    }

    @XmlElement(name = "from")
    public void setFrom(From from) {
        this.from = from;
    }
}

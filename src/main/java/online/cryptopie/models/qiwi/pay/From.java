package online.cryptopie.models.qiwi.pay;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@XmlRootElement(name = "from")
public class From {
    private String ccy;
    private String amount;

    @XmlElement(name = "ccy")
    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    @XmlElement(name = "amount")
    public void setAmount(String amount) {
        this.amount = amount;
    }
}

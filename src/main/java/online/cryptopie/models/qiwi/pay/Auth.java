package online.cryptopie.models.qiwi.pay;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@XmlRootElement(name = "auth")
class Auth {
    private Payment payment;

    @XmlElement(name = "payment")
    public void setPayment(Payment payment) {
        this.payment = payment;
    }
}

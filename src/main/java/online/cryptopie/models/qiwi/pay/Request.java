package online.cryptopie.models.qiwi.pay;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import online.cryptopie.models.qiwi.Password;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@XmlType(propOrder = {"request_type", "terminal_id", "extra_password", "auth"})
@XmlRootElement(name = "request")
public class Request {
    private String request_type;
    private String terminal_id;

    private Password extra_password;
    private Auth auth;

    @SuppressWarnings("Duplicates")
    public static Request buildRequest(String request_type, String terminal_id, String passwordStr, String phoneStr, String card, String ccy, String amount, String serviceId, String transactionNumber) {
        Request request = new Request();
        Auth auth = new Auth();
        Payment payment = new Payment();
        To to = new To();
        From from = new From();
        Account1 account1 = new Account1();
        account1.setCard("");
        account1.setValue(card);
        to.setAmount(amount);
        to.setCcy(ccy);
        to.setService_id(serviceId);
        to.setAccount_number(phoneStr);
        to.setAccount1(account1);

        from.setCcy("EUR");

        payment.setTransaction_number(transactionNumber);
        payment.setTo(to);
        payment.setFrom(from);

        auth.setPayment(payment);

        request.setRequest_type(request_type);
        request.setTerminal_id(terminal_id);
        Password password = new Password();
        password.setPassword("");
        password.setValue(passwordStr);
        request.setExtra_password(password);
        request.setAuth(auth);

        return request;
    }

    public static Request buildRequest(String request_type, String terminal_id, String passwordStr, String phoneStr, String ccy, String amount, String serviceId, String transactionNumber) {
        Request request = new Request();
        Auth auth = new Auth();
        Payment payment = new Payment();
        To to = new To();
        From from = new From();
        to.setAmount(amount);
        to.setCcy(ccy);
        to.setService_id(serviceId);
        to.setAccount_number(phoneStr);

        from.setCcy("EUR");

        payment.setTransaction_number(transactionNumber);
        payment.setTo(to);
        payment.setFrom(from);

        auth.setPayment(payment);

        request.setRequest_type(request_type);
        request.setTerminal_id(terminal_id);
        Password password = new Password();
        password.setPassword("");
        password.setValue(passwordStr);
        request.setExtra_password(password);
        request.setAuth(auth);

        return request;
    }

    @XmlElement(name = "request-type")
    private void setRequest_type(String request_type) {
        this.request_type = request_type;
    }

    @XmlElement(name = "extra")
    private void setExtra_password(Password extra_password) {
        this.extra_password = extra_password;
    }

    @XmlElement(name = "auth")
    private void setAuth(Auth auth) {
        this.auth = auth;
    }

    @XmlElement(name = "terminal-id")
    private void setTerminal_id(String terminal_id) {
        this.terminal_id = terminal_id;
    }
}

package online.cryptopie.models;

import javafx.util.Pair;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor


public class CurrenciesRate {
    Boolean success;
    Long timestamp;
    String base;
    String date;
    List<Pair<String, BigDecimal>> rates;
}

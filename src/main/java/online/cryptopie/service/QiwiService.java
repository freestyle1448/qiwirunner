package online.cryptopie.service;

import online.cryptopie.models.transaction.Transaction;


public interface QiwiService {
    int createQiwiRequest(Transaction transaction);

    int statusRequest(Transaction transaction);

    void saveTransaction(Transaction transaction);

    int declineTransaction(Transaction transaction);

    void ping();
}

package online.cryptopie.service;

import online.cryptopie.app.Locker;
import online.cryptopie.app.QiwiLogger;
import online.cryptopie.app.QiwiProperties;
import online.cryptopie.exception.NotFoundException;
import online.cryptopie.models.Currency;
import online.cryptopie.models.Gate;
import online.cryptopie.models.qiwi.pay.Request;
import online.cryptopie.models.qiwi.payResponse.QiwiResponse;
import online.cryptopie.models.qiwi.ping.BalanceOvd;
import online.cryptopie.models.transaction.Balance;
import online.cryptopie.models.transaction.History;
import online.cryptopie.models.transaction.Transaction;
import online.cryptopie.repositories.CurrenciesRepository;
import online.cryptopie.repositories.GatesRepository;
import online.cryptopie.repositories.ManualRepository;
import online.cryptopie.repositories.TransactionsRepository;
import org.bson.types.ObjectId;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import static online.cryptopie.app.QiwiProperties.WORKER_ID;
import static online.cryptopie.models.transaction.Status.*;


@Service
public class QiwiServiceImpl implements QiwiService {
    private final static String URL = "http://204.155.30.32:8787/request";
    private final static String TEST_CREATE_URL = "http://62.109.31.43:8001/pay";
    private final static String TEST_CHECK_URL = "http://62.109.31.43:8001/check";

    private final QiwiLogger qiwiLogger = QiwiLogger.getInstance();

    private final TransactionsRepository transactionsRepository;
    private final ManualRepository manualRepository;
    private final CurrenciesRepository currenciesRepository;
    private final GatesRepository gatesRepository;
    private final RestTemplate restTemplate;

    private final Locker locker = Locker.getInstance();

    public QiwiServiceImpl(TransactionsRepository transactionsRepository, ManualRepository manualRepository
            , CurrenciesRepository currenciesRepository, GatesRepository gatesRepository, RestTemplate restTemplate) {
        this.transactionsRepository = transactionsRepository;
        this.manualRepository = manualRepository;
        this.currenciesRepository = currenciesRepository;
        this.gatesRepository = gatesRepository;
        this.restTemplate = restTemplate;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public int createQiwiRequest(Transaction transaction) {

        //-------Создание запроса

        StringWriter writer = new StringWriter();
        Request request;
        if (transaction.getCardNumber() != null)
            request = Request.buildRequest("pay",
                    QiwiProperties.terminal_id,
                    QiwiProperties.password,
                    transaction.getUserPhone(),
                    transaction.getCardNumber(),
                    transaction.getAmount().getCurrency(),
                    transaction.getAmount().getAmount().toString(),
                    QiwiProperties.service_id,
                    String.valueOf(transaction.getTransactionNumber()));
        else {
            request = Request.buildRequest("pay",
                    QiwiProperties.terminal_id,
                    QiwiProperties.password,
                    transaction.getUserPhone(),
                    transaction.getAmount().getCurrency(),
                    transaction.getAmount().getAmount().toString(),
                    QiwiProperties.service_id,
                    String.valueOf(transaction.getTransactionNumber()));
        }

        try {
            JAXBContext context = JAXBContext.newInstance(Request.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            // сама сериализация

            marshaller.marshal(request, writer);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        //преобразовываем в строку все записанное в StringWriter
        String result = writer.toString();
        //----------------------------

        //----Получение ответа
        System.out.println(result);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("payload", result);

        HttpEntity<MultiValueMap<String, String>> request1 = new HttpEntity<>(map, headers);

        String response = restTemplate.postForObject(URL, request1, String.class);
        QiwiResponse response1 = null;
        System.err.println(response);
        assert response != null;
        StringReader stringReader = new StringReader(response);
        try {
            JAXBContext context = JAXBContext.newInstance(QiwiResponse.class);
            Unmarshaller jaxbUnmarshaller = context.createUnmarshaller();
            response1 = (QiwiResponse) jaxbUnmarshaller.unmarshal(stringReader);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        //-------------------

        if (response1 != null) {
            if (!response1.getPayment().getFatal_error()) {
                if (!response1.getPayment().getFinal_status())
                    if (response1.getPayment().getStatus() > 49 && response1.getPayment().getStatus() < 60) {
                        qiwiLogger.printMes("CREATE trans num in create " + transaction.getTransactionNumber() + " with status " + response1.getPayment().getStatus());
                        return 0;
                    } else {
                        qiwiLogger.printMes("ERROR while create transaction in create num " + transaction.getTransactionNumber() + " with status " + response1.getPayment().getStatus());
                        return -1;
                    }
                else {
                    if (response1.getPayment().getStatus() == 60) {
                        Optional<Gate> gateOptional = gatesRepository.findById(transaction.getGateId());
                        Optional<Currency> currencyOptional = Optional.empty();
                        if (gateOptional.isPresent())
                            currencyOptional = currenciesRepository.findById(gateOptional.get().getBalance().getCurrency());

                        java.util.Currency currency = java.util.Currency.getInstance(currencyOptional.map(Currency::getCurrency).orElse(null));
                        int cur = currency.getNumericCode();

                        QiwiResponse finalResponse = response1;
                        response1.getBalancesOvd().forEach(balanceOvd1 -> {
                            if (Integer.valueOf(balanceOvd1.getCode()).equals(cur)) {
                                BigDecimal fr = new BigDecimal(finalResponse.getPayment().getFrom().getAmount());
                                BigDecimal t = new BigDecimal(finalResponse.getPayment().getTo().getAmount());
                                BigDecimal rate = t.divide(fr, 6, BigDecimal.ROUND_HALF_UP);
                                Double nC = (transaction.getFinalAmount().getAmount().doubleValue() - transaction.getCommission().getAmount().doubleValue()) - fr.doubleValue();

                                BigDecimal nCBD = new BigDecimal(nC);
                                BigDecimal newCommission = nCBD.setScale(6, RoundingMode.HALF_EVEN);
                                transaction.setCommission(Balance.builder()
                                        .amount((new BigDecimal(transaction.getCommission().getAmount().doubleValue()).add(newCommission)).doubleValue())
                                        .currency(transaction.getCommission().getCurrency())
                                        .build());

                                transaction.setRate(rate.doubleValue());
                                transaction.setSystemAmount(Balance.builder().amount(fr.doubleValue()).currency(currency.getCurrencyCode()).build());

                                confirmTransaction(transaction, balanceOvd1);
                            }
                        });

                        qiwiLogger.printMes("DONE transaction in create num " + transaction.getTransactionNumber() + " with status " + response1.getPayment().getStatus());
                        return 1;
                    } else if (response1.getPayment().getStatus() > 99) {
                        qiwiLogger.printMes("ERROR while processing transaction in create num " + transaction.getTransactionNumber() + " with status " + response1.getPayment().getStatus() + " with message " + response1.getPayment().getMessage());

                        return declineTransaction(transaction.getId(), response1.getPayment().getStatus());
                    }
                }
            } else {
                qiwiLogger.printMes("ERROR while create transaction in create num " + transaction.getTransactionNumber() + " with status " + response1.getPayment().getStatus() + " with message " + response1.getPayment().getMessage());
                return declineTransaction(transaction.getId(), response1.getPayment().getStatus());
            }
        }
        return -1;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public int statusRequest(Transaction transaction) {
        //-------Создание запроса
        StringWriter writer = new StringWriter();
        Request request;
        if (transaction.getCardNumber() != null)
            request = Request.buildRequest("pay",
                    QiwiProperties.terminal_id,
                    QiwiProperties.password,
                    transaction.getUserPhone(),
                    transaction.getCardNumber(),
                    transaction.getAmount().getCurrency(),
                    transaction.getAmount().getAmount().toString(),
                    QiwiProperties.service_id,
                    String.valueOf(transaction.getTransactionNumber()));
        else {
            request = Request.buildRequest("pay",
                    QiwiProperties.terminal_id,
                    QiwiProperties.password,
                    transaction.getUserPhone(),
                    transaction.getAmount().getCurrency(),
                    transaction.getAmount().getAmount().toString(),
                    QiwiProperties.service_id,
                    String.valueOf(transaction.getTransactionNumber()));
        }

        try {
            JAXBContext context = JAXBContext.newInstance(Request.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            // сама сериализация

            marshaller.marshal(request, writer);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        //преобразовываем в строку все записанное в StringWriter
        String result = writer.toString();
        //----------------------------

        //----Получение ответа
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("payload", result);

        HttpEntity<MultiValueMap<String, String>> request1 = new HttpEntity<>(map, headers);

        String response = restTemplate.postForObject(URL, request1, String.class);
        QiwiResponse response1 = null;
        System.err.println(response);
        assert response != null;
        StringReader stringReader = new StringReader(response);
        try {
            JAXBContext context = JAXBContext.newInstance(QiwiResponse.class);
            Unmarshaller jaxbUnmarshaller = context.createUnmarshaller();
            response1 = (QiwiResponse) jaxbUnmarshaller.unmarshal(stringReader);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        //-------------------

        if (response1 != null) {
            if (!response1.getPayment().getFatal_error()) {

                if (!response1.getPayment().getFinal_status()) {
                    if (response1.getPayment().getStatus() > 49 && response1.getPayment().getStatus() < 60) {
                        qiwiLogger.printMes("NOT YET in status req transaction num" + transaction.getTransactionNumber() + " with status " + response1.getPayment().getStatus());
                        return 0;
                    }
                } else {
                    if (response1.getPayment().getStatus() == 60) {
                        Optional<Gate> gateOptional = gatesRepository.findById(transaction.getGateId());
                        Optional<Currency> currencyOptional = Optional.empty();
                        if (gateOptional.isPresent())
                            currencyOptional = currenciesRepository.findById(gateOptional.get().getBalance().getCurrency());

                        java.util.Currency currency = java.util.Currency.getInstance(currencyOptional.map(Currency::getCurrency).orElse(null));
                        int cur = currency.getNumericCode();

                        QiwiResponse finalResponse = response1;
                        response1.getBalancesOvd().forEach(balanceOvd1 -> {
                            if (Integer.valueOf(balanceOvd1.getCode()).equals(cur)) {
                                BigDecimal fr = new BigDecimal(finalResponse.getPayment().getFrom().getAmount());
                                BigDecimal t = new BigDecimal(finalResponse.getPayment().getTo().getAmount());
                                BigDecimal rate = t.divide(fr, 6, BigDecimal.ROUND_HALF_UP);
                                Double nC = (transaction.getFinalAmount().getAmount().doubleValue() - transaction.getCommission().getAmount().doubleValue()) - fr.doubleValue();

                                BigDecimal nCBD = new BigDecimal(nC);
                                BigDecimal newCommission = nCBD.setScale(6, RoundingMode.HALF_EVEN);
                                transaction.setCommission(Balance.builder()
                                        .amount((new BigDecimal(transaction.getCommission().getAmount().doubleValue()).add(newCommission)).doubleValue())
                                        .currency(transaction.getCommission().getCurrency())
                                        .build());

                                transaction.setRate(rate.doubleValue());
                                transaction.setSystemAmount(Balance.builder().amount(fr.doubleValue()).currency(currency.getCurrencyCode()).build());

                                confirmTransaction(transaction, balanceOvd1);
                            }
                        });

                        qiwiLogger.printMes("DONE in status req transaction num" + transaction.getTransactionNumber() + " with status " + response1.getPayment().getStatus());

                    } else if (response1.getPayment().getStatus() > 99) {
                        qiwiLogger.printMes("ERROR in status req while processing transaction num " + transaction.getTransactionNumber() + " with status " + response1.getPayment().getStatus() + " with message " + response1.getPayment().getMessage());

                        return declineTransaction(transaction.getId(), response1.getPayment().getStatus());
                    }
                }
            } else {
                qiwiLogger.printMes("ERROR in status req while processing transaction num " + transaction.getTransactionNumber() + " with status " + response1.getPayment().getStatus() + " with message " + response1.getPayment().getMessage());
                return declineTransaction(transaction.getId(), response1.getPayment().getStatus());
            }
        }

        return -1;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void ping() {

        StringWriter writer = new StringWriter();

        online.cryptopie.models.qiwi.ping.Request request = online.cryptopie.models.qiwi.ping.Request.buildRequest(
                QiwiProperties.password,
                "ping",
                QiwiProperties.terminal_id);

        try {
            JAXBContext context = JAXBContext.newInstance(online.cryptopie.models.qiwi.ping.Request.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            // сама сериализация

            marshaller.marshal(request, writer);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        //преобразовываем в строку все записанное в StringWriter
        String result = writer.toString();
        //----------------------------

        //----Получение ответа


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("payload", result);

        HttpEntity<MultiValueMap<String, String>> request1 = new HttpEntity<>(map, headers);

        String response = restTemplate.postForObject(URL, request1, String.class);


        online.cryptopie.models.qiwi.ping.Response response1 = null;

        assert response != null;
        StringReader stringReader = new StringReader(response);
        try {
            JAXBContext context = JAXBContext.newInstance(online.cryptopie.models.qiwi.ping.Response.class);
            Unmarshaller jaxbUnmarshaller = context.createUnmarshaller();
            response1 = (online.cryptopie.models.qiwi.ping.Response) jaxbUnmarshaller.unmarshal(stringReader);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        //-------------------

        assert response1 != null;
        if (response1.getResult_code().getMsg().equals("Ok"))
            for (BalanceOvd balanceOvd : response1.getBalancesOvd()) {
                manualRepository.findByCodeAndWorkerIdAndModifyBalance(balanceOvd, WORKER_ID);
            }

        locker.unlock(Locker.PING_LOCK);
    }

    @Override
    public void saveTransaction(Transaction transaction) {
        transactionsRepository.save(transaction);
    }

    @Transactional
    public void confirmTransaction(Transaction transaction, BalanceOvd balanceOvd) {
        if (transaction != null) {
            Gate gate = manualRepository.findByCodeAndWorkerIdAndModifyBalance(balanceOvd, WORKER_ID);
            if (gate != null) {

                transaction.getHistoryList().add(History.builder().date(new Date()).prevStatus(transaction.getStatus()).curStatus(SUCCESS).build());

                manualRepository.findAndUpdateGateCommissionAndAccount(gate.getId(), transaction.getCommission());
                transaction.setStatus(SUCCESS);
                transactionsRepository.save(transaction);

                qiwiLogger.printMes("Transaction accepted");
            } else {
                qiwiLogger.printMes("IN TRANSACTION SAVE not enough money on gate or gate with id dont find");
            }
        } else {
            qiwiLogger.printMes("IN TRANSACTION SAVE transaction dont find");
        }
    }

    @SuppressWarnings("Duplicates")
    @Transactional
    public int declineTransaction(ObjectId transactionId, Integer remoteStatus) {
        Transaction transaction = transactionsRepository.findByIdAndStatus(transactionId, IN_PROCESS);
        if (transaction != null) {
            if ((manualRepository.findAndModifyAccountAdd(transaction.getAccountId(), transaction.getFinalAmount()) != null)
                    && (manualRepository.findAndModifyGateAdd(transaction.getGateId(), transaction.getFinalAmount()) != null)) {
                if (transaction.getHistoryList() == null) {
                    transaction.setHistoryList(new ArrayList<>());
                    transaction.getHistoryList()
                            .add(History.builder()
                                    .date(new Date())
                                    .prevStatus(transaction.getStatus())
                                    .curStatus(DENIED)
                                    .build());
                } else {
                    transaction.getHistoryList()
                            .add(History.builder()
                                    .date(new Date())
                                    .prevStatus(transaction.getStatus())
                                    .curStatus(DENIED)
                                    .build());
                }

                transaction.setStatus(DENIED);
                transaction.setRemoteStatus(remoteStatus);
                transactionsRepository.save(transaction);

                qiwiLogger.printMes("Transaction declined");
                return 1;
            } else {
                qiwiLogger.printMes("IN TRANSACTION SAVE gate with id dont find");
                throw new NotFoundException("IN TRANSACTION SAVE gate with id dont find");

            }
        } else {
            qiwiLogger.printMes("IN TRANSACTION SAVE transaction dont find");
            throw new NotFoundException("IN TRANSACTION SAVE gate with id dont find");
        }
    }

    @SuppressWarnings("Duplicates")
    @Transactional
    public int declineTransaction(Transaction transaction) {
        if (transaction != null) {
            if ((manualRepository.findAndModifyAccountAdd(transaction.getAccountId(), transaction.getFinalAmount()) != null)
                    && (manualRepository.findAndModifyGateAdd(transaction.getGateId(), transaction.getFinalAmount()) != null)) {
                if (transaction.getHistoryList() == null) {
                    transaction.setHistoryList(new ArrayList<>());
                    transaction.getHistoryList()
                            .add(History.builder()
                                    .date(new Date())
                                    .prevStatus(transaction.getStatus())
                                    .curStatus(DENIED)
                                    .build());
                } else {
                    transaction.getHistoryList().
                            add(History.builder()
                                    .date(new Date())
                                    .prevStatus(transaction.getStatus())
                                    .curStatus(DENIED)
                                    .build());
                }

                transaction.setStatus(DENIED);
                transactionsRepository.save(transaction);

                qiwiLogger.printMes("Transaction declined");
                return 1;
            } else {
                qiwiLogger.printMes("IN TRANSACTION SAVE gate with id dont find");
                throw new NotFoundException("IN TRANSACTION SAVE gate with id dont find");
            }
        } else {
            qiwiLogger.printMes("IN TRANSACTION SAVE transaction dont find");
            throw new NotFoundException("IN TRANSACTION SAVE gate with id dont find");
        }
    }
}

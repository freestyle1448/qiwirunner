package online.cryptopie.tasks;

import online.cryptopie.app.Locker;
import online.cryptopie.models.AutomaticGate;
import online.cryptopie.models.transaction.Transaction;
import online.cryptopie.repositories.AutomaticGatesRepository;
import online.cryptopie.repositories.ManualRepository;
import online.cryptopie.repositories.QiwiCounterRepository;
import online.cryptopie.service.QiwiServiceImpl;
import org.bson.types.ObjectId;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.Collections;

import static online.cryptopie.app.Locker.PAY_LOCK;
import static online.cryptopie.app.Locker.PING_LOCK;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class QiwiTasksTest {
    private static QiwiTasks qiwiTasks;
    private static AutomaticGatesRepository automaticGatesRepository = mock(AutomaticGatesRepository.class);
    private static ManualRepository manualRepository = mock(ManualRepository.class);
    private static QiwiServiceImpl qiwiService = mock(QiwiServiceImpl.class);
    private final static Locker locker = Locker.getInstance();
    private static QiwiCounterRepository qiwiCounterRepository = mock(QiwiCounterRepository.class);
    private ArgumentCaptor argCaptor = ArgumentCaptor.forClass(Transaction.class);

    @BeforeClass
    public static void init() {
        qiwiTasks = new QiwiTasks(manualRepository, automaticGatesRepository, qiwiService);
    }

    @Test
    public void checkTransactionsOnQiwiTest() {
        when(automaticGatesRepository.findByGroupName(anyString())).thenReturn(AutomaticGate.builder().gates(Collections.singletonList(ObjectId.get())).build());
        when(manualRepository.findByStatusQIWITransactions(any(), any())).thenReturn(Collections.singletonList(Transaction.builder().build()));

        qiwiTasks.checkTransactionsOnQiwi();
        verify(qiwiService).statusRequest(any());
    }

    @Test
    public void pingTest() {
        qiwiTasks.ping();
        verify(qiwiService).ping();
        locker.unlock(PING_LOCK);
    }

    @Test
    public void pay() {
        when(automaticGatesRepository.findByGroupName(anyString())).thenReturn(AutomaticGate.builder().gates(Collections.singletonList(ObjectId.get())).build());
        when(manualRepository.findByStatusQIWITransactions(any(), any())).thenReturn(Collections.singletonList(Transaction.builder().build()));
        //when(qiwiCounterRepository.findAll()).thenReturn(Collections.singletonList(Counter.builder().count(0).build()));
        qiwiTasks.pay();

        verify(qiwiService).saveTransaction((Transaction) argCaptor.capture());
        assertEquals(((Transaction) argCaptor.getValue()).getStatus().intValue(), 1);
        locker.unlock(PAY_LOCK);
    }
}

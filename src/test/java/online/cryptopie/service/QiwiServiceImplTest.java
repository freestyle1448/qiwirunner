package online.cryptopie.service;


import online.cryptopie.dto.BalanceDTO;
import online.cryptopie.models.Account;
import online.cryptopie.models.Currency;
import online.cryptopie.models.Gate;
import online.cryptopie.models.transaction.Balance;
import online.cryptopie.models.transaction.History;
import online.cryptopie.models.transaction.Transaction;
import online.cryptopie.repositories.CurrenciesRepository;
import online.cryptopie.repositories.GatesRepository;
import online.cryptopie.repositories.ManualRepository;
import online.cryptopie.repositories.TransactionsRepository;
import org.bson.types.ObjectId;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static online.cryptopie.models.transaction.Status.DENIED;
import static online.cryptopie.models.transaction.Status.SUCCESS;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


public class QiwiServiceImplTest {
    private static RestTemplate restTemplate = mock(RestTemplate.class);
    private static ManualRepository manualRepository = mock(ManualRepository.class);
    private static TransactionsRepository transactionsRepository = mock(TransactionsRepository.class);
    private static GatesRepository gatesRepository = mock(GatesRepository.class);
    private static CurrenciesRepository currenciesRepository = mock(CurrenciesRepository.class);
    private static QiwiServiceImpl qiwiService;

    @Captor
    private ArgumentCaptor trCaptor = ArgumentCaptor.forClass(Transaction.class);

    @BeforeClass
    public static void init() {
        qiwiService = new QiwiServiceImpl(transactionsRepository, manualRepository, currenciesRepository, gatesRepository, restTemplate);
    }

    @Test
    public void createQiwiRequestTest() {
        when(restTemplate.postForObject(anyString(), any(), any()))
                .thenReturn("<response><result-code fatal=\"false\">0</result-code><payment status='50' final-status='false' fatal-error='false'><from><amount>9.89</amount><ccy>978</ccy></from><to><amount>742.35</amount><ccy>643</ccy></to></payment></response>");
        when(manualRepository.findAndModifyGateSub(any(), any()))
                .thenReturn(Gate.builder().build());


        qiwiService.createQiwiRequest(Transaction.builder()
                .finalAmount(Balance.builder().amount(100).currency("RUB").build())
                .build());

        assertEquals(qiwiService.createQiwiRequest(Transaction.builder()
                .finalAmount(Balance.builder().amount(100).currency("RUB").build())
                .build()), 0);
    }

    @Test
    public void createQiwiRequestTestDone() {
        when(restTemplate.postForObject(anyString(), any(), any()))
                .thenReturn("<response><result-code fatal=\"false\">0</result-code><payment status='60' final-status='true' fatal-error='false'><from><amount>9.89</amount><ccy>978</ccy></from><to><amount>742.35</amount><ccy>643</ccy></to></payment><balances-ovd><balance-ovd code=\"978\">1112.68</balance-ovd></balances-ovd></response>");
        when(manualRepository.findAndModifyGateSub(any(), any())).thenReturn(Gate.builder().build());
        when(manualRepository.findByCodeAndWorkerIdAndModifyBalance(any())).thenReturn(Gate.builder().build());
        when(gatesRepository.findById(any())).thenReturn(Optional.of(Gate.builder().balance(BalanceDTO.builder().currency(ObjectId.get()).build()).build()));
        when(currenciesRepository.findById(any())).thenReturn(Optional.of(Currency.builder().currency("EUR").build()));
        List<History> list = new ArrayList<>();
        qiwiService.createQiwiRequest(Transaction.builder()
                .amount(Balance.builder().amount(100).currency("RUB").build())
                .commission(Balance.builder().amount(1).currency("RUB").build())
                .finalAmount(Balance.builder().amount(100).currency("RUB").build())
                .historyList(list)
                .build());

        verify(transactionsRepository, atLeastOnce()).save((Transaction) trCaptor.capture());
        assertEquals(((Transaction) trCaptor.getValue()).getStatus(), SUCCESS);
    }

    @Test
    public void createQiwiRequestTestWithFatalError() {
        when(restTemplate.postForObject(anyString(), any(), any()))
                .thenReturn("<response><result-code fatal=\"false\">0</result-code><payment status='50' final-status='false' fatal-error='true'><from><amount>9.89</amount><ccy>978</ccy></from><to><amount>742.35</amount><ccy>643</ccy></to></payment></response>");
        when(manualRepository.findAndModifyGateSub(any(), any()))
                .thenReturn(Gate.builder().build());
        when(transactionsRepository.findByIdAndStatus(any(), anyInt())).thenReturn(Transaction.builder().build());
        when(manualRepository.findAndModifyAccountAdd(anyString(), any())).thenReturn(Account.builder().build());
        when(manualRepository.findAndModifyGateAdd(any(), any())).thenReturn(Gate.builder().build());

        qiwiService.createQiwiRequest(Transaction.builder()
                .finalAmount(Balance.builder().amount(100).currency("RUB").build())
                .build());

        verify(transactionsRepository, atLeastOnce()).save((Transaction) trCaptor.capture());
        assertEquals(((Transaction) trCaptor.getValue()).getStatus(), DENIED);
    }

    @Test
    public void statusRequestTest() {
        when(restTemplate.postForObject(anyString(), any(), any()))
                .thenReturn("<response><result-code fatal=\"false\">0</result-code><payment status='50' final-status='false' fatal-error='false'><from><amount>9.89</amount><ccy>978</ccy></from><to><amount>742.35</amount><ccy>643</ccy></to></payment></response>");
        when(manualRepository.findAndModifyGateSub(any(), any()))
                .thenReturn(Gate.builder().build());

        assertEquals(qiwiService.statusRequest(Transaction.builder()
                .finalAmount(Balance.builder().amount(100).currency("RUB").build())
                .build()), 0);
    }

    @Test
    public void statusRequestTestDone() {
        when(restTemplate.postForObject(anyString(), any(), any()))
                .thenReturn("<response><result-code fatal=\"false\">0</result-code><payment status='60' final-status='true' fatal-error='false'><from><amount>9.89</amount><ccy>978</ccy></from><to><amount>742.35</amount><ccy>643</ccy></to></payment><balances-ovd><balance-ovd code=\"978\">1112.68</balance-ovd></balances-ovd></response>");
        when(manualRepository.findAndModifyGateSub(any(), any())).thenReturn(Gate.builder().build());
        when(manualRepository.findByCodeAndWorkerIdAndModifyBalance(any())).thenReturn(Gate.builder().build());
        when(gatesRepository.findById(any())).thenReturn(Optional.of(Gate.builder().balance(BalanceDTO.builder().currency(ObjectId.get()).build()).build()));
        when(currenciesRepository.findById(any())).thenReturn(Optional.of(Currency.builder().currency("EUR").build()));
        List<History> list = new ArrayList<>();
        qiwiService.statusRequest(Transaction.builder()
                .amount(Balance.builder().amount(100).currency("RUB").build())
                .commission(Balance.builder().amount(1).currency("RUB").build())
                .finalAmount(Balance.builder().amount(100).currency("RUB").build())
                .historyList(list)
                .build());

        verify(transactionsRepository, atLeastOnce()).save((Transaction) trCaptor.capture());
        assertEquals(((Transaction) trCaptor.getValue()).getStatus(), SUCCESS);
    }

    @Test
    public void statusRequestTestWithFatalError() {
        when(restTemplate.postForObject(anyString(), any(), any()))
                .thenReturn("<response><result-code fatal=\"false\">0</result-code><payment status='50' final-status='false' fatal-error='true'><from><amount>9.89</amount><ccy>978</ccy></from><to><amount>742.35</amount><ccy>643</ccy></to></payment></response>");
        when(manualRepository.findAndModifyGateSub(any(), any()))
                .thenReturn(Gate.builder().build());
        when(transactionsRepository.findByIdAndStatus(any(), anyInt())).thenReturn(Transaction.builder().build());
        when(manualRepository.findAndModifyAccountAdd(anyString(), any())).thenReturn(Account.builder().build());
        when(manualRepository.findAndModifyGateAdd(any(), any())).thenReturn(Gate.builder().build());

        qiwiService.statusRequest(Transaction.builder()
                .finalAmount(Balance.builder().amount(100).currency("RUB").build())
                .build());

        verify(transactionsRepository, atLeastOnce()).save((Transaction) trCaptor.capture());
        assertEquals(((Transaction) trCaptor.getValue()).getStatus(), DENIED);
    }

    @Test
    public void pingTest() {
        when(restTemplate.postForObject(anyString(), any(), any()))
                .thenReturn("<response><result-code fatal=\"false\" message=\"Ok\" msg=\"Ok\">0</result-code><balances><balance code=\"643\">0.00</balance><balance code=\"978\">873.06</balance></balances><balances-ovd><balance-ovd code=\"643\">0.00</balance-ovd><balance-ovd code=\"978\">200.00</balance-ovd></balances-ovd><f></f><lk-status>001</lk-status></response>");
        when(gatesRepository.findByCodeAndWorkerId(anyInt(), anyInt())).thenReturn(Gate.builder().balance(BalanceDTO.builder().amount(100).build()).build());
        when(manualRepository.findByCodeAndWorkerIdAndModifyBalance(any())).thenReturn(Gate.builder().balance(BalanceDTO.builder().amount(200).build()).build());
        when(currenciesRepository.findById(any())).thenReturn(Optional.of(Currency.builder().currency("EUR").build()));

        qiwiService.ping();

        verify(transactionsRepository).save((Transaction) trCaptor.capture());
        assertEquals(((Transaction) trCaptor.getValue()).getFinalAmount().getAmount().doubleValue(), 100, 2);
    }

    @Test
    public void pingTestWithoutAddTrans() {
        when(restTemplate.postForObject(anyString(), any(), any()))
                .thenReturn("<response><result-code fatal=\"false\" message=\"Ok\" msg=\"Ok\">0</result-code><balances><balance code=\"643\">0.00</balance><balance code=\"978\">873.06</balance></balances><balances-ovd><balance-ovd code=\"643\">0.00</balance-ovd><balance-ovd code=\"978\">200.00</balance-ovd></balances-ovd><f></f><lk-status>001</lk-status></response>");
        when(gatesRepository.findByCode(anyInt())).thenReturn(Gate.builder().balance(BalanceDTO.builder().amount(200).build()).build());
        when(manualRepository.findByCodeAndWorkerIdAndModifyBalance(any())).thenReturn(Gate.builder().balance(BalanceDTO.builder().amount(100).build()).build());

        qiwiService.ping();

    }
}
